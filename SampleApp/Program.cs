﻿using System;

namespace SampleApp
{
    public class Program
    {
        private static void Main(string[] args)
        {
            Program obj = new Program();
            Program obj2 = new Program();
            int x, y;
            x = 1; y = 1;
            string a = obj.ToString();
            string b = obj2.ToString();
            obj2.ToString();
            Console.WriteLine("Object comparison");
            Console.WriteLine(obj == obj2);
            Console.WriteLine(a == b);
            Console.WriteLine(a.Equals(b));
            Console.WriteLine("Equals method-" + obj.Equals(obj2));
            Console.WriteLine("X and Y comparison");
            Console.WriteLine(x == y);
            Console.WriteLine(x = y);
            Console.WriteLine("Equals method-" + x.Equals(y));
            x = +2;
            y = -2;
            Console.WriteLine(--x);
            Console.WriteLine(x++);
            Console.ReadLine();
        }
    }

    //public class NewProgram : Program
    //{
    //  private static void Main(string[] args)
    //  {
    //    NewProgram obj1 = new NewProgram();
    //    Program obj = new Program();
    //    Console.WriteLine(obj1.Equals(obj));
    //    Console.ReadLine();
    //  }
    //}
}
